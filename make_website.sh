#!/bin/bash
jupyter nbconvert --to html --execute --ExecutePreprocessor.timeout=3000 --no-input --template classic --log-level WARN index.ipynb >> error.log 2>&1
rsync -ravz index.html uploader@qt4.tudelft.net:
rsync -ravz database.p uploader@qt4.tudelft.net:
rm -f index.html
